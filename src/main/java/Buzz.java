public class Buzz {

    private static final int BUZZ=5;


    public static boolean isBUZZ(int number) {
        return number % BUZZ == 0 ;
    }

    public static String returnBuzz (int number) {
        return isBUZZ(number) ? "Buzz" : "" ;
    }
}
