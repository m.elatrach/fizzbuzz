import java.io.StringWriter;

public class FizzBuzz {

    StringBuilder result =new StringBuilder();

    public FizzBuzz(int start, int end) {

        for(int i=start;i<=end;i++)
            result.append(i+ ": " + Fizz.returnFizz(i) + Buzz.returnBuzz(i) + "\n");

    }


    public void generate(StringWriter out){
        out.write(result.toString());

    }
}
