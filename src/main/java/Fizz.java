public class Fizz {

    private static final int FIZZ=3;

    public static boolean isFIZZ(int number) {
        return number % FIZZ == 0;
    }

    public static String returnFizz (int number) {

        return isFIZZ(number)?"Fizz":"" ;
    }

}
